const express = require('express');
const router = express.Router();
const { logger, authentication } = require('../middlewares/middleware');
const customerRoutes = require('./customers.routes');
const orderRoutes = require('./orders.routes')
const productRoutes = require('./products.routes');

router.use(logger);
router.use(authentication);
router.get('/', (req, res) => {
    res.status(200).json({
        endpoints: {
            customers: '/customers',
            orders: '/orders',
            products: '/products'
        }
    });
});
router.use('/customers', customerRoutes);
router.use('/orders', orderRoutes);
router.use('/products', productRoutes);
router.use((req, res) => {
    res.status(404).json({
        status: 'Not Found',
        message: 'Alamat tidak ditemukan'
    });
});

module.exports = router;