const express = require('express');
const router = express.Router();
const orderController = require('../controllers/orders.controller');

router.get('/', orderController.getAllorders);
router.get('/:id', orderController.getorderById);
router.post('/', orderController.createorder);
router.patch('/:id', orderController.updateorder);
router.delete('/:id', orderController.deleteorder);

module.exports = router;