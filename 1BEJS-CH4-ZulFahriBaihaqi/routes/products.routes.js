const express = require('express');
const router = express.Router();
const productController = require('../controllers/products.controller');

router.get('/', productController.getAllproducts);
router.get('/:id', productController.getproductById);
router.post('/', productController.createproduct);
router.patch('/:id', productController.updateproduct);
router.delete('/:id', productController.deleteproduct);

module.exports = router;