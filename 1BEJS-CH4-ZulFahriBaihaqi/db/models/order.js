'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class order extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      order.belongsTo(models.customer);
      models.customer.hasMany(order);
    }
  }
  order.init({
    quantity: DataTypes.INTEGER,
    order_date: DataTypes.DATE
  }, {
    sequelize,
    modelName: 'order',
  });
  return order;
};