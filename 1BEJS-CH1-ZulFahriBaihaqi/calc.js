//Zul Fahri Baihaqi

const rl = require('readline').createInterface({
    input: process.stdin,
    output: process.stdout
});

const { promisify } = require('util'); //harus banyak baca lagi
const question = promisify(rl.question).bind(rl);

async function operasiHitung(operator){
    let pilihan = false;
    do{
        const operasi = await question(`\nOperasi hitung apa yang ingin dijalankan?
        \rPilihan:
        \rPenjumlahan  (${operator[0]})
        \rPengurangan  (${operator[1]})
        \rPerkalian    (${operator[2]})
        \rPembagian    (${operator[3]})
        \rAkar Kuadrat (${operator[4]})
Masukkan simbol (+, -, *, /, sqrt): `);

        if(operator.includes(operasi)){
            pilihan = true;
            let input1 = await question('Masukkan bilangan pertama: ');
            let input2 = await question('Masukkan bilangan kedua: ');
            if(isNaN(input1) || isNaN(input2)){
                console.log('\nBilangan hanya dapat berupa angka.');
                pilihan = false;
            }else{
                switch(operasi){
                    case '+':
                        hasil = +input1 + +input2;
                        console.log(`\nHasil dari ${input1} + ${input2} adalah ${hasil}.`);
                        break;
                    case '-':
                        hasil = +input1 - +input2;
                        console.log(`\nHasil dari ${input1} - ${input2} adalah ${hasil}.`);
                        break;
                    case '*':
                        hasil = +input1 * +input2;
                        console.log(`\nHasil dari ${input1} * ${input2} adalah ${hasil}.`);
                        break;
                    case '/':
                        hasil = +input1 / +input2;
                        console.log(`\nHasil dari ${input1} / ${input2} adalah ${hasil}.`);
                        break;
                    case 'sqrt':
                        hasil = +input1 * Math.sqrt(+input2);
                        console.log(`\nHasil dari ${input1} √ (${input2}) adalah ${hasil}.`);
                        break;
                    default:
                        console.log('\nTerjadi kesalahan.');
                };
            };
        }else{
            console.log('\nHarap masukkan simbol operator yang sesuai.');
        };
    }while(!pilihan);
    rl.close();
};

operasiHitung(['+', '-', '*', '/', 'sqrt']);