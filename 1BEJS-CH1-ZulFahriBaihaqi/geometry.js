//Zul Fahri Baihaqi

const rl = require('readline').createInterface({
    input: process.stdin,
    output: process.stdout
});

const { promisify } = require('util');;
const question = promisify(rl.question).bind(rl);

async function hitungGeometri(choice){
    let pilihan = false;
    do{
        const choose = await question(`Operasi hitung apa yang ingin dijalankan?
        \rHitung luas persegi  [${choice[0]}]
        \rHitung volume kubus  [${choice[1]}]
        \rHitung volume tabung [${choice[2]}]
Masukkan pilihan (1, 2, 3): `);

        if(choice.includes(choose)){
            switch (choose){
                case '1':
                    await luasPersegi();
                    pilihan = true;
                    break;
                case '2':
                    await volumeKubus();
                    pilihan = true;
                    break;
                case '3':
                    await volumeTabung();
                    pilihan = true;
                    break;
                default:
                    console.log('Terjadi kesalahan.');
            };
        }else{
            console.log('Harap masukkan pilihan yang sesuai.');
            pilihan = false;
        };
    }while(!pilihan);
    rl.close();
};

async function luasPersegi(){
    let s1 = await question('Masukkan panjang sisi persegi: ');
    const rumusLP = s1 ** 2;

    if(!isNaN(s1)){
        console.log(`Luas perseginya adalah ${rumusLP}.`);
    }else{
        console.log('Panjang sisi hanya dapat berupa angka.');
    };
};

async function volumeKubus(){
    let s = await question('Masukkan panjang sisi kubus: ');
    const rumusVK = s ** 3;

    if(!isNaN(s)){
        console.log(`Volume kubusnya adalah ${rumusVK}.`);
    }else{
        console.log('Panjang sisi hanya dapat berupa angka.')
    };
};

async function volumeTabung(){
    let r = await question('Masukkan panjang jari-jari tabung: ');
    const pi = 3.14;
    const rumusVT = pi * (r ** 2);

    if(!isNaN(r)){
        console.log(`Volume tabungnya adalah ${rumusVT}.`);
    }else{
        console.log('Jari-jari hanya dapat berupa angka.')
    };
};

hitungGeometri(['1', '2', '3']);