const rl = require('readline').createInterface({
    input: process.stdin,
    output: process.stdout
});
const { promisify } = require('util');
const question = promisify(rl.question).bind(rl);

class nilaiSiswa {
    constructor(nama){
        this.nama = nama;
    };

    async hitung(){
        let nilai = [];
        let input = true;
        let i = 0;
        while(input){
            if(!(nilai.includes('q'))){
                try{
                    i++;
                    nilai.push(await question(`Masukkan nilai siswa ke ${i}: `));
                }catch(err){
                    console.error('Terjadi kesalahan.', err);
                };
            }else{
                nilai.pop();
                nilai = nilai.map(i => Number(i));
                nilai = nilai.filter(i => !(isNaN(i)) && !(i < 0) && !(i > 100));
                rl.close();
                input = false;
            };
        };
        return this.announce(nilai);
    };

    announce(nilai){
        let mean = (nilai.reduce((a, b) => a + b, 0) / nilai.length).toFixed(1);
        let lulus = [];
        let tidakLulus = [];

        nilai.forEach(element => {
            if(element >= 75){
                lulus.push(element);
            }else{
                tidakLulus.push(element);
            };
        });

        console.log(`\n\t\tData nilai ${this.nama}
        \rNilai tertinggi           : ${Math.max(...nilai)}
        \rNilai terendah            : ${Math.min(...nilai)}
        \rNilai rata - rata         : ${mean}
        \rJumlah siswa lulus        : ${lulus.length} orang (${lulus.sort((a, b) => b - a)})
        \rJumlah siswa tidak lulus  : ${tidakLulus.length} orang (${tidakLulus.sort((a, b) => b - a)})
        \rUrutan nilai              : ${nilai.sort((a, b) => b - a)}
        \rNilai yang tidak valid akan otomatis terhapus (negatif atau lebih dari 100).`);
    };
};

let kelas1 = new nilaiSiswa('Kelas A');
kelas1.hitung();