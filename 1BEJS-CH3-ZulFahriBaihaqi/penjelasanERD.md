## Database: cafebejs1

```
Etitas      : Profiles
Primary Key : profile_id
Foreign Key : -
Attributes  : (PK)profile_id, email, phone_num
Relasi      : Profiles dapat memiliki satu Crews (One to One)

Etitas      : Positions
Primary Key : position_id
Foreign Key : -
Attributes  : (PK)profile_id, email, position_name
Relasi      : Positions dapat memiliki banyak Crews (One to Many)

Etitas      : Crews
Primary Key : crew_id
Foreign Key : profile_id, position_id
Attributes  : (PK)crew_id, crew_name, (FK)position_id, (FK)profile_id
Relasi      : Satu Crews dapat dimiliki oleh satu Profiles (One to One), 
            Banyak Crews dapat dimiliki oleh satu Positions (One to Many)
```