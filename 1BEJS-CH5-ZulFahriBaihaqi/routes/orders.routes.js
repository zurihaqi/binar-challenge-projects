const express = require('express');
const router = express.Router();
const orderController = require('../controllers/orders.controller');
const { orderValidator } = require('../validator/order.validator')

router.get('/', orderController.getAllorders);
router.get('/:id', orderController.getorderById);
router.post('/', orderValidator, orderController.createorder);
router.patch('/:id', orderValidator, orderController.updateorder);
router.delete('/:id', orderController.deleteorder);

module.exports = router;