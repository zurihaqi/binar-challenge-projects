require('dotenv').config();
const express = require('express');
const router = require('./routes/index.routes');
const app = express();
const port = process.env.PORT;

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(`/`, router);

app.listen(port, () => {
    console.log(`Listening on port ${port}`);
});


/**
 * sequelize db:create
 * sequelize db:migrate
 * sequelize db:seed:all
 * npm start
 * 
 * swagger-ui: /api-docs
 */
