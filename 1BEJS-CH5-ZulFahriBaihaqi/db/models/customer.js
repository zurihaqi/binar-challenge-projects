'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class customer extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      
    }
  }
  customer.init({
    cust_name: DataTypes.STRING,
    email: DataTypes.STRING,
    phone_num: DataTypes.BIGINT
  }, {
    sequelize,
    modelName: 'customer',
  });
  return customer;
};