const { product, order } = require('../db/models');

module.exports = class productController{
    static options = {
        attributes: {
            exclude: ['createdAt', 'updatedAt']
        },
        include: [{
            model: order,
            attributes:{
                exclude: ['createdAt', 'updatedAt']
            }
        }]
    }

    static async getAllproducts(req, res){
        try{
            let {page, row} = req.query;
            page -= 1;
    
            if(page) productController.options.offset = page;
            if(row) productController.options.limit = +row;
    
            const allproducts = await product.findAll(productController.options);
    
            return res.status(200).json({
                status: 'Success',
                data: allproducts
            });
        }catch(error){
            return res.status(500).json({
                status: 'error',
                message: error.message});
        };
    };
    
    static async getproductById(req, res){
        try{
            const found = await product.findByPk(req.params.id, productController.options);
    
            if(found){
                return res.status(200).json({
                    status: 'Success',
                    data: found
                });
            };
            return res.status(404).json({
                status: 'Not Found',
                message: `Product dengan id ${req.params.id} tidak ditemukan.`
            });
        }catch(error){
            return res.status(500).json({
                status: 'error',
                message: error.message});
        };
    };
    
    static async createproduct(req, res){
        try{
            const {product_name, price, orderId} = req.body;
            const productExists = await product.findOne({where: {product_name: product_name}});
            if(productExists){
                return res.status(409).json({
                    status:'Error',
                    message:`Produk dengan nama ${product_name} sudah terdaftar dalam database.`
                });
            };
            const created = await product.create({
                product_name: product_name,
                price: price,
                orderId: orderId
            });
            return res.status(200).json({
                status: 'Berhasil membuat data.',
                data: created
            });
        }catch(error){
            return res.status(500).json({
                status: 'error',
                message: error.message});
        };
    };
    
    static async updateproduct(req, res){
        try{
            const {product_name, price, orderId} = req.body;
            const orderExists = await order.findByPk(orderId);
            if(!orderExists){
                 return res.status(404).json({
                    status: 'Error',
                    message: `Order dengan id ${orderId} tidak ada dalam database.`
                });
            };
            const updated = await product.update({
                product_name: product_name,
                price: price,
                orderId: orderId
            }, {
                where: {
                    id: req.params.id
                }
            });
            if(updated){
                const updatedProduct = await product.findByPk(req.params.id, productController.options);
                if(updatedProduct){
                    return res.status(200).json({
                        status: `Berhasil mengupdate data dengan id ${req.params.id}`,
                        data: updatedProduct
                    });
                };
                return res.status(404).json({
                    status: 'Not Found',
                    message: `Product dengan id ${req.params.id} tidak ditemukan.`
                });
            };
        }catch(error){
            return res.status(500).json({
                status: 'error',
                message: error.message});
        };
    };
    
    static async deleteproduct(req, res){
        try{
            const deleted = await product.destroy({
                where: {id: req.params.id}
            });
            if(deleted) return res.status(200).json({
                status: 'Sukses',
                message: `Product dengan id ${req.params.id} berhasil dihapus.`
            });
            return res.status(404).json({
                status: 'Not Found',
                message: `Product dengan id ${req.params.id} tidak ditemukan.`
            });
        }catch(error){
            return res.status(500).json({
                status: 'error',
                message: error.message});
        };
    };
};