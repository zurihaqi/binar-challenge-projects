const { customer, order } = require('../db/models');

module.exports = class customerController {
    static options = {
        attributes: {
            exclude: ['createdAt', 'updatedAt']
        }
    };

    static async getAllCustomers(req, res){
        try{
            let {page, row} = req.query;
            page -= 1;
    
            if(page) customerController.options.offset = page;
            if(row) customerController.options.limit = +row;
    
            const allCustomers = await customer.findAll(customerController.options);
    
            return res.status(200).json({
                status: 'Success',
                data: allCustomers
            });
        }catch(error){
            return res.status(500).json({
                status: 'Error',
                message: error.message});
        };
    };

    static async getCustomerById(req, res){
        try{
            const found = await customer.findByPk(req.params.id, customerController.options);
    
            if(found){
                return res.status(200).json({
                    status: 'Success',
                    data: found
                });
            };
            return res.status(404).json({
                status: 'Not Found',
                message: `Customer dengan id ${req.params.id} tidak ditemukan.`,
            });
        }catch(error){
            return res.status(500).json({
                status: 'Error',
                message: error.message});
        };
    };
    static async createCustomer(req, res){
        try{
            const {cust_name, email, phone_num} = req.body;
            const emailExists = await customer.findOne({where: {email: email}});
            if(emailExists){
                return res.status(409).json({
                    status: 'Error',
                    message: 'Alamat email sudah terdaftar'
                });
            };
            const created = await customer.create({
                cust_name: cust_name,
                email: email,
                phone_num: phone_num
            });
            return res.status(200).json({
                status: 'Berhasil membuat data.',
                data: created
            });
        }catch(error){
            return res.status(500).json({
                status: 'Error',
                message: error.message});
        };
    };
    
    static async updateCustomer(req, res){
        try{
            const {cust_name, email, phone_num} = req.body;
            const updated = await customer.update({
                cust_name: cust_name,
                email: email,
                phone_num: phone_num
            }, {
                where: {
                    id: req.params.id
                }
            });
            if(updated){
                const updatedCust = await customer.findByPk(req.params.id, customerController.options);
                if(updatedCust){
                    return res.status(200).json({
                        status: `Berhasil mengupdate data customer dengan id ${req.params.id}`,
                        data: updatedCust
                    });
                };
                return res.status(404).json({
                    status: 'Not Found',
                    message: `Customer dengan id ${req.params.id} tidak ditemukan.`,
                });
            };
        }catch(error){
            return res.status(500).json({
                status: 'Error',
                message: error.message});
        };
    };
    
    static async deleteCustomer(req, res){
        try{
            const foundFK = await order.findOne({where: {customerId: req.params.id}});
            if(foundFK){
                return res.status(422).json({
                    status: 'Error',
                    message: `Customer dengan id ${req.params.id} masih terpakai pada data order berikut.`,
                    data: foundFK
                });
            };
            const deleted = await customer.destroy({
                where: {id: req.params.id}
            });
            if(deleted) return res.status(200).json({
                status: `Sukses`,
                message: `Customer dengan id ${req.params.id} berhasil dihapus.`
            });
            return res.status(404).json({
                status: 'Not Found',
                message: `Customer dengan id ${req.params.id} tidak ditemukan.`,
            });
        }catch(error){
            return res.status(500).json({
                status: 'Error',
                message: error.message});
        };
    };
};