const { order, customer, product } = require('../db/models');

module.exports = class orderController{
    static options = {
        attributes: {
            exclude: ['createdAt', 'updatedAt']
        },
        include: [{
            model: customer,
            attributes:{
                exclude: ['createdAt', 'updatedAt']
            }
        }]
    };

    static async getAllorders(req, res){
        try{
            let {page, row} = req.query;
            page -= 1;
    
            if(page) orderController.options.offset = page;
            if(row) orderController.options.limit = +row;
    
            const allorders = await order.findAll(orderController.options);
    
            return res.status(200).json({
                status: 'Success',
                data: allorders
            });
        }catch(error){
            return res.status(500).json({
                status: 'error',
                message: error.message});
        };
    };
    
    static async getorderById(req, res){
        try{
            const found = await order.findByPk(req.params.id, orderController.options);
    
            if(found){
                return res.status(200).json({
                    status: 'Success',
                    data: found
                });
            }
            return res.status(404).json({
                status: 'Not Found',
                message: `Order dengan id ${req.params.id} tidak ditemukan.`
            });
        }catch(error){
            return res.status(500).json({
                status: 'error',
                message: error.message});
        };
    };
    
    static async createorder(req, res){
        try{
            const {quantity, order_date, customerId} = req.body;
            const custExists = await customer.findByPk(customerId);
            if(!custExists){
                return res.status(404).json({
                    status: 'Error',
                    message: `Customer dengan id ${customerId} tidak ada dalam database.`
                });
            };
            const created = await order.create({
                quantity: quantity,
                order_date: order_date,
                customerId: customerId
            });
            return res.status(200).json({
                status: 'Berhasil membuat data.',
                data: created
            });
        }catch(error){
            return res.status(500).json({
                status: 'error',
                message: error.message});
        };
    };
    
    static async updateorder(req, res){
        try{
            const {quantity, order_date, customerId} = req.body;
            const custExists = await customer.findByPk(customerId);
            if(!custExists){
                 return res.status(404).json({
                    status: 'Error',
                    message: `Customer dengan id ${customerId} tidak ada dalam database.`
                });
            };
            const updated = await order.update({
                quantity: quantity,
                order_date: order_date,
                customerId: customerId
            }, {
                where: {
                    id: req.params.id
                }
            });
            if(updated){
                const updatedOrder = await order.findByPk(req.params.id, orderController.options);
                if(updatedOrder){
                    return res.status(200).json({
                        status: `Berhasil mengupdate data dengan id ${req.params.id}`,
                        data: updatedCust
                    });
                };
                return res.status(404).json({
                    status: 'Not Found',
                    message: `Order dengan id ${req.params.id} tidak ditemukan.`
                });
            };
        }catch(error){
            return res.status(500).json({
                status: 'error',
                message: error.message});
        };
    };
    
    static async deleteorder(req, res){
        try{
            const foundFK = await product.findOne({where: {orderId: req.params.id}});
            if(foundFK){
                return res.status(422).json({
                    status: 'Error',
                    message: `Order dengan id ${req.params.id} masih terpakai pada data produk berikut.`,
                    data: foundFK
                })
            };
            const deleted = await order.destroy({
                where: {id: req.params.id}
            });
            if(deleted) return res.status(200).json({
                status: 'Sukses',
                message: `Order dengan id ${req.params.id} berhasil dihapus.`
            });
            return res.status(404).json({
                status: 'Not Found',
                message: `Order dengan id ${req.params.id} tidak ditemukan.`
            });
        }catch(error){
            return res.status(500).json({
                status: 'error',
                message: error.message});
        };
    };
};