const { body, validationResult } = require('express-validator');

module.exports.cust_validator = [
    body('cust_name')
    .isAlpha('en-US', {ignore: ' '})
    .withMessage('Nama tidak boleh mengandung angka atau kosong.'),
    body('email')
    .isEmail()
    .normalizeEmail()
    .withMessage('Harap masukkan alamat email yang sesuai.'),
    body('phone_num')
    .isMobilePhone()
    .withMessage('Harap masukkan nomor telpon yang sesuai.'),
    (req, res, next) => {
        const errors = validationResult(req);
        if(!errors.isEmpty()){
            return res.status(400).json({
                errors: errors.array()
            });
        };
        next();
    }
];