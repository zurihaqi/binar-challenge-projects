const { body, validationResult } = require('express-validator');

module.exports.orderValidator = [
    body('quantity').isNumeric().withMessage('Quantity hanya dapat berupa angka.'),
    body('order_date').isDate().withMessage('Order date hanya dapat berupa tanggal.'),
    body('customerId').isNumeric().withMessage('Customer id hanya dapat berupa angka.'),
    (req, res, next) => {
        const errors = validationResult(req);
        if(!errors.isEmpty()){
            return res.status(400).json({
                errors: errors.array()
            });
        };
        next();
    }
];